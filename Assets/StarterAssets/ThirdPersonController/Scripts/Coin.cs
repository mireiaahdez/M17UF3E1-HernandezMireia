using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public bool rotate;
    public float rotationSpeed;

    // Update is called once per frame
    void Update()
    {
        if (rotate)
            transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime, Space.World);
    }


    private void OnTriggerEnter(Collider other)
    {
        
        GameManager.CoinsAmount -= 1;
        SpawnCoin.canSpawn = true;
        Destroy(gameObject);
    }

   
}

