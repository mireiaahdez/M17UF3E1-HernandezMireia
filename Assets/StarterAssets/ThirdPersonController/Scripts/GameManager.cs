using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static int CoinsAmount = 10;
    public TextMeshProUGUI  coinTexts;

    // Start is called before the first frame update
    void Start()
    {
        coinTexts.text = CoinsAmount.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        coinTexts.text = CoinsAmount.ToString();
        if(CoinsAmount == 0)
        {
            SceneManager.LoadScene("GameWin");
        }
    }

    public void GameReset()
    {
        Time.timeScale = 1f;
        CoinsAmount = 15;
        SceneManager.LoadScene("Playground");
    }

    public void GameQuit()
    {
        Application.Quit();
    }
}
