using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCoin : MonoBehaviour
{
    public GameObject coin;
    public static bool canSpawn;
    public float raycastLenght = 15f;

    // Update is called once per frame
    void Update()
    {
        if (canSpawn)
        {
            Vector3 randPos = new Vector3(Random.Range(-10f, 20f), 0f, Random.Range(-1f, -30f));
            RaycastHit hit;
            if(Physics.Raycast(randPos, -Vector3.up, out hit, raycastLenght))
            {
                if (hit.collider.gameObject.GetComponent<MeshCollider>() != null || hit.collider.gameObject.GetComponent<BoxCollider>() != null)
                {
                    Instantiate(coin, hit.point + new Vector3(0f, 1.2f, 0f), Quaternion.identity);
                    canSpawn = false;
                }
            }
        }
    }
}
